<?php

namespace App;

class User extends BaseAuth
{
    public function getNameAttribute()
    {
        return '[User] ' . $this->attributes['name'];
    }
}
