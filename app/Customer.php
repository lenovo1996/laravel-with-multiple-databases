<?php

namespace App;

class Customer extends BaseAuth
{
    public function getNameAttribute()
    {
        return '[Customer] ' . $this->attributes['name'];
    }
}
