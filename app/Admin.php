<?php

namespace App;

class Admin extends BaseAuth
{
    public function getNameAttribute()
    {
        return '[Admin] ' . $this->attributes['name'];
    }
}
